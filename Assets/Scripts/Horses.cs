﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class Horses : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f;
    private bool run = false;
    private bool run_two = false;
    // Update is called once per frame

    void Start()
    {
        GameManager inst = GameManager.GetInstance();
        inst.StopHorses += ToggleRun;
        inst.IncreaseHorse();
    }
    void Update()
    {
        if (run)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
    }

    public void ToggleRun()
    {
        run = !run;
    }

    public void ToggleRunTwo()
    {
        run = !run;
        run_two = !run_two;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Trigger Last"))
        {
            // ToggleRun();
            if (!run_two)
            {
                GameManager.GetInstance().NotifyWon();
            }
        }

        if (run_two)
        {
            if (other.gameObject.CompareTag("Trigger"))
            {
                NotifyCheckpoint();
                StartCoroutine(WaitForOthers());
            }
            else if (other.gameObject.CompareTag("Trigger Last"))
            {
                ToggleRun();
            }

        }
        go(sum(5, 10));
    }

    IEnumerator WaitForOthers()
    {
        while (!GameManager.GetInstance().HasOtherPassed())
        {
            run = false;
            yield return null;
        }

        run = true;
    }

    private void NotifyCheckpoint()
    {
        GameManager.GetInstance().Notify();
    }

    public async void go(Task<int> sum)
    {
        var t = await sum;
        Debug.Log(t);
    }

    private Task<int> sum(int a, int b)
    {
        return Task.Factory.StartNew(() => { 
            Debug.Log(System.Threading.Thread.CurrentThread.Name);
            Thread.Sleep(1000);
            return a + b; 
        });
    }

    // private Task testTask()
}
