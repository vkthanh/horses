﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [SerializeField] private float speed = 4.0f;
    [SerializeField] private float slowSpeed = 1.5f;
    private bool move = false;

    // Update is called once per frame
    void Update()
    {
        if (move)
            transform.Translate(Vector3.right * speed * Time.deltaTime);
    }

    public void ToggleMove()
    {
        move = !move;
    }

    public void ToggleMoveSlow()
    {
        move = !move;
        
        speed = speed + slowSpeed;
        slowSpeed = speed - slowSpeed;
        speed = speed - slowSpeed;
    }

    public void StopCamera()
    {
        move = false;
    }
}
