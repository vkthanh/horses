﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public Action StopHorses;
    private int horse_num = 0;
    private int horse_at_check_point = 0;
    // private bool horse1__passed_checkpoint = false;
    // private bool horse2__passed_checkpoint = false;

    // private bool horse3__passed_checkpoint = false;
    // Update is called once per frame
    private GameManager() {}

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public bool HasOtherPassed()
    {
        return horse_at_check_point == horse_num;
    }

    public void Notify()
    {
        horse_at_check_point++;

        if (horse_at_check_point >= horse_num)
        {
            StartCoroutine(WaitOneTick());
        }
    }
    
    public void IncreaseHorse()
    {
        ++horse_num;
    }

    public void NotifyWon()
    {
        StopHorses();
        Camera.main.GetComponent<CameraMove>().StopCamera();
    }
    public static GameManager GetInstance()
    {
        return instance;
    }

    IEnumerator WaitOneTick()
    {
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        horse_at_check_point = 0;
    }
}
